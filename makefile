LIBS = -lUltralight -lUltralightCore -lWebCore -lAUL -lSDL2 -lAppCore
CFLAGS = -g -Wall -Wl,-rpath='libs/'
BUILD_PATH = build
LIB_PATH = lib
SRC_PATH = src
INCLUDES = -I ${LIB_PATH}/AUL3/include -I ${LIB_PATH}/Ultralight/include -I ${SRC_PATH}
CC = gcc ${CFLAGS} ${INCLUDES} -L ${LIB_PATH}/AUL3/build -L ${LIB_PATH}/Ultralight/bin

SOURCE := $(wildcard $(SRC_PATH)/*.c)
OBJECTS := $(SOURCE:$(SRC_PATH)/%.c=$(BUILD_PATH)/objects/%.o)

CMD_PREFIX := @

GREEN = `tput setaf 2`
BLUE = `tput setaf 4`

.DEFAULT: all
all: dirs aul $(BUILD_PATH)/acl

.PHONY: clean
clean:
	@echo -e "${GREEN}- Cleaning up build files..."
	@rm -rf $(BUILD_PATH)

.PHONY: dirs
dirs:
	@echo -e "${GREEN}- Creating directories..."
	@mkdir -p $(BUILD_PATH)
	@mkdir -p $(BUILD_PATH)/objects
	@mkdir -p $(BUILD_PATH)/libs

.PHONY: aul
aul:
	@echo -e "${GREEN}- Building AUL3..."
	@make -s -C ${LIB_PATH}/AUL3
	@echo -e "${GREEN}- Copying Libraries..."
	@cp ${LIB_PATH}/AUL3/build/libAUL.so ${BUILD_PATH}/libs
	@cp ${LIB_PATH}/Ultralight/bin/libUltralightCore.so ${BUILD_PATH}/libs
	@cp ${LIB_PATH}/Ultralight/bin/libUltralight.so ${BUILD_PATH}/libs
	@cp ${LIB_PATH}/Ultralight/bin/libWebCore.so ${BUILD_PATH}/libs
	@cp ${LIB_PATH}/Ultralight/bin/libAppCore.so ${BUILD_PATH}/libs
	@cp -r ${LIB_PATH}/Ultralight/bin/resources ${BUILD_PATH}

$(BUILD_PATH)/objects/%.o: $(SRC_PATH)/%.c
	@echo "${GREEN}- Compiling: ${BLUE}$< ${GREEN}-> ${BLUE}$@..."
	$(CMD_PREFIX)$(CC) -c $< -o $@

$(BUILD_PATH)/acl: $(OBJECTS)
	@echo -e "${GREEN}- Linking: ${BLUE}$@..."
	$(CMD_PREFIX)$(CC) $^ $(LIBS) -o $@
