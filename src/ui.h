#pragma once
#include <SDL2/SDL.h>
#include <Ultralight/CAPI.h>
#include <AppCore/CAPI.h>
#include "surface.h"

int ACL_UIInit();

void ACL_UIUpdate();

void ACL_UIHandleEvents(bool *exit);

void ACL_UIShutdown();
