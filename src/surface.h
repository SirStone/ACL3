#pragma once
#include <SDL2/SDL.h>
#include <Ultralight/CAPI.h>

void* ACL_CreateSurface(unsigned int width, unsigned int height);

void ACL_DestroySurface(void* surface);

unsigned int ACL_GetSurfaceHeight(void* surface);

unsigned int ACL_GetSurfaceRowBytes(void* surface);

unsigned long ACL_GetSurfaceSize(void* surface);

unsigned int ACL_GetSurfaceWidth(void* surface);

void* ACL_LockSurface(void* surface);

void ACL_ResizeSurface(void* surface, unsigned int width, unsigned int height); 

void ACL_UnlockSurface(void* surface);

void ACL_CreateSurfaceDefinition(ULSurfaceDefinition *surfaceDef);
