#pragma once
#include <JavaScriptCore/JavaScript.h>
#include <Ultralight/CAPI.h>
#include <AUL/AUL3.h>

void DoExit(bool *exit);

JSValueRef DoExitCallback(JSContextRef ctx, JSObjectRef function,
  JSObjectRef thisObject, size_t argumentCount, const JSValueRef arguments[],
  JSValueRef* exception);

void OnDOMReady(void* user_data, ULView caller, unsigned long long frame_id,
                bool is_main_frame, ULString url);
