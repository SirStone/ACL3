#include "js_callbacks.h"

extern ULView mainView;
static bool js_exit = false;
static char workshop_dir[512] = {};
static aul_PresetData* preset;

void DoExit(bool *exit) {
	if(js_exit) {
		*exit = true;
	}
}

JSValueRef DoExitCallback(JSContextRef ctx, JSObjectRef function,
  JSObjectRef thisObject, size_t argumentCount, const JSValueRef arguments[],
  JSValueRef* exception) {

	js_exit = true;

	return 0;
}

JSValueRef DoPrintCallback(JSContextRef ctx, JSObjectRef function,
		JSObjectRef thisObject, size_t argumentCount, const JSValueRef arguments[],
		JSValueRef* exception) {

	printf("Printed\n");

	return 0;
}

JSValueRef DoPopulateModListInactiveCallback(JSContextRef ctx, JSObjectRef function,
		JSObjectRef thisObject, size_t argumentCount, const JSValueRef arguments[],
		JSValueRef* exception) {

	JSValueRef mod_names[preset->modCount];
	JSObjectRef mod_array;
	for (int i=0; i<preset->modCount; i++) {
		mod_names[i] = JSValueMakeString(ctx, JSStringCreateWithUTF8CString(preset->mods[i].mod_name));
	}
	mod_array = JSObjectMakeArray(ctx, preset->modCount, mod_names, NULL);
	printf("Running DoPopulateModListInactiveCallback\n");

	return mod_array;
}

static void registerFunctionCallback(JSContextRef* ctx, const char* functionName, JSObjectCallAsFunctionCallback function) {
	JSStringRef name = JSStringCreateWithUTF8CString(functionName);
	JSObjectRef func = JSObjectMakeFunctionWithCallback(*ctx, name, function);
	JSObjectSetProperty(*ctx, JSContextGetGlobalObject(*ctx), name, func, 0, 0);
	JSStringRelease(name);
}

void OnDOMReady(void* user_data, ULView caller, unsigned long long frame_id,
                bool is_main_frame, ULString url) {
		
	if (getenv("AUL3_WORKSHOP_DIR") != NULL) {
		strcpy(workshop_dir, getenv("AUL3_WORKSHOP_DIR"));
	}
	else {	
		strcat(workshop_dir, getenv("HOME"));
		strcat(workshop_dir, "/.local/share/Steam/steamapps/workshop/content/107410/");
	}
	preset = aul_CreatePresetData("My Preset", aul_GetSubscribedMods(workshop_dir, NULL));

	JSContextRef ctx = ulViewLockJSContext(mainView);
	
	///
	/// Create a JavaScript String containing the name of our callback.
	///
	//JSStringRef name = JSStringCreateWithUTF8CString("DoExitCallback");
	
	///
	/// Create a garbage-collected JavaScript function that is bound to our
	/// native C callback 'GetMessage()'.
	///
	//JSObjectRef func = JSObjectMakeFunctionWithCallback(ctx, name, DoExitCallback);
	
	///
	/// Store our function in the page's global JavaScript object so that it
	/// accessible from the page as 'GetMessage()'.
	///
	/// The global JavaScript object is also known as 'window' in JS.
	///
	//JSObjectSetProperty(ctx, JSContextGetGlobalObject(ctx), name, func, 0, 0);
	
	///
	/// Release the JavaScript String we created earlier.
	///
	//JSStringRelease(name);
	
	// My Function
	registerFunctionCallback(&ctx, "DoExitCallback", DoExitCallback);
	registerFunctionCallback(&ctx, "DoPopulateModListInactiveCallback", DoPopulateModListInactiveCallback);
	registerFunctionCallback(&ctx, "DoPrintCallback", DoPrintCallback);
	
	///
	/// Unlock the JS context so other threads can modify JavaScript state.
	///
	ulViewUnlockJSContext(mainView);
}
