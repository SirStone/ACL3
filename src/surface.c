#include "surface.h"

void* ACL_CreateSurface(unsigned int width, unsigned int height) {	
	return SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, SDL_PIXELFORMAT_BGRA32);
}

void ACL_DestroySurface(void* surface) {
	printf("Running DestroySurface callback\n");
	SDL_FreeSurface(surface);
}

unsigned int ACL_GetSurfaceHeight(void* surface) {	
	return ((SDL_Surface*) surface)->h;
}

unsigned int ACL_GetSurfaceRowBytes(void* surface) {
	return ((SDL_Surface*) surface)->w * 4;
}

unsigned long ACL_GetSurfaceSize(void* surface) {
	printf("Running GetSurfaceSize callback\n");
	return sizeof(SDL_Surface);
}

unsigned int ACL_GetSurfaceWidth(void* surface) {	
	printf("Running GetSurfaceWidth callback: %i\n", ((SDL_Surface*) surface)->w);
	return ((SDL_Surface*) surface)->w;
}

void* ACL_LockSurface(void* surface) {
	SDL_LockSurface((SDL_Surface*) surface);
	return ((SDL_Surface*) surface)->pixels;
}

void ACL_ResizeSurface(void* surface, unsigned int width, unsigned int height) {
	SDL_Surface* newSurface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, SDL_PIXELFORMAT_BGRA32);

	SDL_BlitScaled(newSurface, NULL, (SDL_Surface*) surface, NULL);
}

void ACL_UnlockSurface(void* surface) {	
	SDL_UnlockSurface((SDL_Surface*) surface);
}

void ACL_CreateSurfaceDefinition(ULSurfaceDefinition *surfaceDef) {
	surfaceDef->create = &ACL_CreateSurface;
	surfaceDef->destroy = &ACL_DestroySurface;
	surfaceDef->get_height = &ACL_GetSurfaceHeight;
	surfaceDef->get_row_bytes = &ACL_GetSurfaceRowBytes;
	surfaceDef->get_size = &ACL_GetSurfaceSize;
	surfaceDef->get_width = &ACL_GetSurfaceWidth;
	surfaceDef->lock_pixels = &ACL_LockSurface;
	surfaceDef->resize = &ACL_ResizeSurface;
	surfaceDef->unlock_pixels = &ACL_UnlockSurface;
}
