#include "ui.h"
#include "js_callbacks.h"

static SDL_Window* mainWindow = NULL;
static SDL_Surface* mainWindowSurface = NULL;
static ULRenderer mainRenderer = NULL;
ULView mainView = NULL;
static ULSurface mainViewSurface = NULL;
static ULMouseEvent mouse = NULL;
static int mouse_x = 0;
static int mouse_y = 0;

int ACL_UIInit() {

	int width = 1024;
	int height = 768;
	ULSurfaceDefinition surfaceDef = {};

	if (SDL_Init(SDL_INIT_VIDEO)) {
		printf("Error: SDL2 could not initialize\n");
		return 1;
	}
	else {
		mainWindow = SDL_CreateWindow("Arma 3 Commander Launcher", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN | 
				SDL_WINDOW_BORDERLESS);
		if (mainWindow == NULL) {
			printf("Error: SDL2 could not create window\n");	
			return 1;
		}
		else {
			mainWindowSurface = SDL_GetWindowSurface(mainWindow);
			if (mainWindowSurface == NULL) {
				printf("Error: SDL2 could not get surface from window\n");
				return 1;
			}
		}
	}
	
	ACL_CreateSurfaceDefinition(&surfaceDef);
	ulPlatformSetSurfaceDefinition(surfaceDef);
	ulEnableDefaultLogger(ulCreateString("./runtime.log"));
	ulEnablePlatformFileSystem(ulCreateString("."));
	ulEnablePlatformFontLoader();
	
	ULConfig config = ulCreateConfig();
	ulConfigSetResourcePath(config, ulCreateString("./resources/"));
	ulConfigSetUseGPURenderer(config, false);
	ulConfigSetDeviceScale(config, 1.0);
	mainRenderer = ulCreateRenderer(config);
	
	mainView = ulCreateView(mainRenderer, width, height, false, NULL, false);
	ulViewLoadURL(mainView, ulCreateString("file:///assets/app.html"));
	ulViewSetDOMReadyCallback(mainView, OnDOMReady, NULL);
	mainViewSurface = ulViewGetSurface(mainView);

	ulDestroyConfig(config);

	return 0;
}

void ACL_UIUpdate() {
	ulViewFocus(mainView);
	ulUpdate(mainRenderer);
	ulRender(mainRenderer);
	SDL_BlitSurface((SDL_Surface*) ulSurfaceGetUserData(mainViewSurface), NULL, mainWindowSurface, NULL);
	SDL_UpdateWindowSurface(mainWindow);	
}

void ACL_UIHandleEvents(bool *exit) {
	SDL_Event e;
	while(SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT) {
			*exit = true;
		}
		else {
			switch (e.type) {
				case SDL_MOUSEMOTION:
					SDL_GetMouseState(&mouse_x, &mouse_y);
					mouse = ulCreateMouseEvent(kMouseEventType_MouseMoved, mouse_x, mouse_y, kMouseButton_None);
					ulViewFireMouseEvent(mainView, mouse);
					break;
				case SDL_MOUSEBUTTONDOWN:
					SDL_GetMouseState(&mouse_x, &mouse_y);
					mouse = ulCreateMouseEvent(kMouseEventType_MouseDown, mouse_x, mouse_y, kMouseButton_Left);
					ulViewFireMouseEvent(mainView, mouse);
					break;
				case SDL_MOUSEBUTTONUP:
					SDL_GetMouseState(&mouse_x, &mouse_y);
					mouse = ulCreateMouseEvent(kMouseEventType_MouseUp, mouse_x, mouse_y, kMouseButton_Left);
					ulViewFireMouseEvent(mainView, mouse);
					break;
			}
		}
	}
	DoExit(exit);
}

void ACL_UIShutdown() {
	ulDestroyMouseEvent(mouse);
	ulDestroyView(mainView);
	ulDestroyRenderer(mainRenderer);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
}
