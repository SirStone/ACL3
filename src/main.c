#include <AUL/AUL3.h>
#include "ui.h"

int main()
{
	if(ACL_UIInit()) {
		ACL_UIShutdown();
		return 1;
	}
	else {
		bool exit = false;
		while(!exit) {
			ACL_UIHandleEvents(&exit);
			ACL_UIUpdate();
		}
		ACL_UIShutdown();
	}
	return 0;
}
