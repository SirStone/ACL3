var should_update = true;

function openMenu(menuName) {
	var i, menus, tabbar, tabbed, tablinks, tabmain;
	
	// Get all elements with class="tabcontent" and hide them
	menus = document.getElementsByClassName("menu-item");
	tabbar = document.getElementsByClassName("tabbar");
	tabbed = document.getElementsByClassName("tabbed");
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < menus.length; i++) {
		menus[i].style.display = "none";
	}	
	for (i = 0; i < tabbar.length; i++) {
		tabbar[i].style.display = "none";	
	}
	for (i = 0; i < tabbed.length; i++) {
		tabbed[i].style.display = "none";
	}
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	document.getElementById(menuName).style.display = "block";
	document.getElementById(menuName+"-tab").style.display = "block";
	document.getElementById("tabmain-"+menuName).style.display = "block";

	tabmain = document.getElementsByClassName("tab-main");
	for (i = 0; i < tabmain.length; i++) {
		tabmain[i].className += " active";
	}
}

function switch_mod(mod_item) {
	active_mod_list = document.getElementById("active-mods")
	inactive_mod_list = document.getElementById("inactive-mods")
	var moveTo = mod_item.parentElement.parentElement == inactive_mod_list ? active_mod_list : inactive_mod_list;	
	moveTo.prepend(mod_item.parentElement);

	if (active_mod_list.childElementCount == 0) {
		active_mod_list.style.border.bottom = "none";
		active_mod_list.style.display = "none";
	} else {
		active_mod_list.style.border.bottom = "2px solid #FF9900";
		active_mod_list.style.display = "block";
	}	
}

function populate_modlist() {
	if (should_update) {
		mod_array = DoPopulateModListInactiveCallback();
		inactive_mod_list = document.getElementById("inactive-mods");
		for (i = 0; i < mod_array.length; i++) {
			var entry = document.createElement("label");
			entry.className = "checkbox-label";
			entry.id = "mod"+i;
			entry.innerHTML = "<input class='check-input hide' type='checkbox' onchange='switch_mod(this)'><span class='checkbox')></span><b class='checkbox-text'>"+mod_array[i]+"</b>";
			inactive_mod_list.appendChild(entry);
		}
		var items = inactive_mod_list.children;
		items = Array.prototype.slice.call(items).sort(function(a, b) {
		    return a.id.localeCompare(b.id);
		});
		
		for (var i = 0, length = items.length; i < length; i++) {
		    inactive_mod_list.appendChild(items[i]);
		}
		should_update = false;
	}
}

document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
	openMenu("news");
  }
};


function openTab(evt, tabName) {
	// Declare all variables
	var i, tabbed, tablinks;
	
	// Get all elements with class="tabcontent" and hide them
	tabbed = document.getElementsByClassName("tabbed");
	for (i = 0; i < tabbed.length; i++) {
	  tabbed[i].style.display = "none";
	}
	
	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
	  tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	
	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(tabName).style.display = "block";
	evt.currentTarget.className += " active";
}
